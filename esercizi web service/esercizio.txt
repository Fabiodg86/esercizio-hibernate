
   Creare un web service rest come quelli di esempio svolti nella lezione,
   visualizzando nella pagina del browser una liste di tecnologie o linguaggi
   piu apprezzati durante l'academy utilizzando il metodo Get.



   Esempio

   
   import java.util.ArrayList;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.servizi.rest.User;

@RestController
@RequestMapping("/api")
public class RestControl {
	
	private List<User> theUsers;
	
	@GetMapping("/users")
	public List<Student> getUsers(){
		
		List<User> theUsers = new ArrayList<>();
		
		theUsers.add(new User("Mario","Rossi"));
		theUsers.add(new User("Antonio","Bianchi"));
		theUsers.add(new User("Giuseppe","Verdi"));
		
		return theUsers;
	}
	  @GetMapping("/users/{userId}")
      public User getUser(@PathVariable int userId) {
    	  
    	  return theUsers.get(userId);
    	  
    	  
      }    
	
	
	
	
}